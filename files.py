from os import listdir, path
from random import randrange
from fpdf import FPDF
import locale
loc = locale.getlocale()
locale.setlocale(locale.LC_ALL, 'pt_BR')

class DadosPdf:
    def __init__(self):
        self.BASE_DIR = path.dirname(__file__)
        self.template = path.join(self.BASE_DIR, 'imagem\\1.jpg')
        self.BASE_SAIDA = path.join(self.BASE_DIR, 'saida')
        
        # Create FPDF object
        # Layout ('P','L')
        # Unit('mm','cm','in')
        # format ('A3','A4',(default), 'A5', 'Letter', 'Legal',(100,150))
        self.pdf = FPDF('P', 'mm', 'A4')
        # Add a page
        self.pdf.add_page()
        
        # Add image no background
        #self.pdf.image(self.template, x=0, y=0, w=round(self.pdf.w), h=round(self.pdf.h))
        #self.pdf.set_page_background(self.template)
        
        # specify font
        # fonts ('times', 'courier', 'helvetica' , 'symbol', zpfdingbats')
        # 'B' (bold), 'U' (underline, 'I' (italics), ''(regular), combination (i.e., ('BU'))
        self.pdf.set_font('Times', 'BI', 11)

        self.matriz()
    
    def matriz(self):
        i = 0
        for i in range(1, 251):
            p1 = i
            p2 = i + 250
            p3 = i + 500
            p4 = i + 750
            self.posicoes(p1, p2, p3, p4)
            #self.pdf.add_page()
            i+=1
        
        self.salva_pdf("TV-IMG-2")

    def posicoes(self, p1, p2, p3, p4):
        p1 = p1 ; p2 = p2 ; p3 = p3 ; p4 = p4

        self.ENTRE_TITULO = self.pdf.h/4
        # PRIMEIRO TITULO
        self.pdf.text(11, 8, str(p1).zfill(6)) # CANHOTO
        self.pdf.text(55, 66.5, str(p1).zfill(6)) # TITULO
        
        # SEGUNDO TITULO
        self.pdf.text(11, ( (self.ENTRE_TITULO) + 8), str(p2).zfill(6)) # CANHOTO
        self.pdf.text(55, ( (self.ENTRE_TITULO) + 66.5), str(p2).zfill(6)) # TITULO
        
        # TERCEIRO TITULO
        self.pdf.text(11, ( (self.ENTRE_TITULO * 2) + 8), str(p3).zfill(6)) # CANHOTO
        self.pdf.text(55, ( (self.ENTRE_TITULO * 2) + 66.5), str(p3).zfill(6)) # TITULO
        
        # QUARTO TITULO
        self.pdf.text(11, ( (self.ENTRE_TITULO * 3) + 8), str(p4).zfill(6)) # CANHOTO
        self.pdf.text(55, ( (self.ENTRE_TITULO * 3) + 66.5), str(p4).zfill(6)) # TITULO

        self.pdf.add_page()
        # Add image no background
        #self.pdf.image(self.template, x=0, y=0, w=round(self.pdf.w), h=round(self.pdf.h))


    def salva_pdf(self,nome=''):
        self.nome = f"Ação_Entre_Amigos_pag_{nome}"
        self.pdf.output(path.join(self.BASE_SAIDA, self.nome + ".pdf"))


if __name__ == '__main__':
    DadosPdf()